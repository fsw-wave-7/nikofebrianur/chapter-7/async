const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };
  
  let fridge = ["Apple", "Wortel", "Milk", "Orange", "Leech", "Cabbage"];
  
  // To randomly set the egg availability
  if (Math.floor(Math.random() * 1) == 1) {
    fridge.push("Egg");
  }
  
  function checkTheEggAvailability() {
    return new Promise(async (resolve) => {
      console.clear();
      console.log("Checking the egg availability...");
      await sleep(2000);
  
      // Check the egg availability
      for (let i = 0; i < fridge.length; i++) {
        if (fridge[i] == "Egg") {
          console.log("There's an egg!");
          resolve(false);
        }
      }
  
      // If we complete the loop and there's no egg
      console.log("There's no egg!");
      await sleep(1000);
      resolve(true);
    });
  }
  
  // Function to handle if we have to go to market or not
  async function goToTheMarket(shouldWe) {
    if (shouldWe) {
      console.clear();
      console.log("Heading to the market...");
      await sleep(4000);
      console.log("Buying an egg...");
      await sleep(1000);
    }
  }
  
  // Function to prepare the fry
  async function prepareTheFry() {
    console.clear();
    console.log("Pouring the oil...");
    await sleep(1000);
  
    console.clear();
    console.log("Lit up the stove...");
    await sleep(1000);
  }
  
  // Fry the egg
  function fryTheEgg() {
    return new Promise((resolve) => {
      console.clear();
      console.log("Cook the egg...");
      sleep(4000).then(() => {
        console.clear();
        resolve("Egg is ready to eat!");
      });
    });
  }
  
  // Go to kitchen and check the fridge
  async function cookAnEgg() {
    // Check if the egg is available
    let shouldWeGoToTheMarket = await checkTheEggAvailability();
  
    // Check if we should go to the market or not
    await goToTheMarket(shouldWeGoToTheMarket);
    await prepareTheFry();
  
    console.log(await fryTheEgg());
  }
  
  cookAnEgg();
  